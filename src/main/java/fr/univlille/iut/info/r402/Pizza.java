package fr.univlille.iut.info.r402;

public class Pizza {
    public static final int DUREE_CUISSON = 5;
    private Recette recette;
    private int tickCuisson;
    public Pizza(Recette recette){
        this.tickCuisson = 0;
        this.recette = recette;
    }

    public boolean estCuite(){
        return tickCuisson >= DUREE_CUISSON;
    }

    public void cuire(){
        tickCuisson ++;
    }
}
