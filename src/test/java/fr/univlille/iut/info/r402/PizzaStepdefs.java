package fr.univlille.iut.info.r402;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PizzaStepdefs {
    Four four;
    Pizzaiolo pizzaiolo;
    Pizza maPizza;

    @Given("^un four")
    public void unFour() {
        four = new Four();
    }

    @And("^un pizzaiolo")
    public void unPizzaiolo() {
        pizzaiolo = new Pizzaiolo();
    }

    @And("^le pizzaiolo prépare une pizza reine")
    public void preparerReine() {
        maPizza = pizzaiolo.preparerPizza(Recette.REINE);
    }
    @When("^le pizzaiolo met la pizza reine au four$")
    public void pizzaAuFour() {
         pizzaiolo.mettreDansFour(maPizza, four);
    }

    @Then("au bout de {int} ticks d'horloge, la pizza est cuite")
    public void pizzaCuite(int ticks) {
        try {
            Thread.sleep(Pizza.DUREE_CUISSON * Four.TICK_DELAY + Four.TICK_DELAY);
            assertTrue(maPizza.estCuite());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}