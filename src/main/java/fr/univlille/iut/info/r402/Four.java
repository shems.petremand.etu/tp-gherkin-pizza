package fr.univlille.iut.info.r402;

import java.util.ArrayList;
import java.util.List;

public class Four {
    private List<Pizza> pizzas;
    private static final int CAPACITE = 3;
    public static final int TICK_DELAY = 1000;
    private Thread tCuisson;

    public Four(){
        pizzas = new ArrayList<>();
        this.allumer();
    }

    public void allumer(){
        tCuisson = new Thread(() -> {
            try {
                while(true){
                    Thread.sleep(TICK_DELAY);
                    for(Pizza pizza : pizzas){
                        pizza.cuire();
                    }
                }
            } catch (InterruptedException e) {
                //Eh
            }
        });
        tCuisson.start();
    }

    public void eteindre(){
        tCuisson.interrupt();
    }

    public void enfourner(Pizza pizza){
        pizzas.add(pizza);
    }
}
