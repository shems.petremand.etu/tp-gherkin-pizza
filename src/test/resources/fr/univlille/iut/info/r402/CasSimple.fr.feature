# language: fr
# https://cucumber.io/docs/gherkin/reference/
Fonctionnalité: Ma première fonctionalité :
  En tant que joueur
  je peux préparer une pizza
  afin de la vendre

  Scénario: Un pizza reine
    Étant donné un four
    Et un pizzaiolo
    Et le pizzaiolo prépare une pizza reine
    Quand le pizzaiolo met la pizza reine au four
    Alors au bout de 30 ticks d'horloge, la pizza est cuite