package fr.univlille.iut.info.r402;

public class Pizzaiolo {
    public void mettreDansFour(Pizza pizza, Four four){
        four.enfourner(pizza);
    }

    public Pizza preparerPizza(Recette recette){
        return new Pizza(recette);
    }
}
